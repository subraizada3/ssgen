import os

import markdown

from python.core.config import Config

# caches
raw_file_cache = {}
compiled_md_cache = {}
metadata_cache = {}

from python.core.md_builder import md


# load all md and html files into caches
def init_caches():
	# content - process markdown files only
	for rootpath, _, filenames in os.walk(Config.content_path):
		for filename in filenames:
			if not filename.endswith(".md"):
				continue
			path = rootpath + "/" + filename
			file = open(path, 'r').read()
			raw_file_cache[path] = file
			compiled_md_cache[path] = md.convert(file)
			metadata_cache[path] = md.Meta

	# pages - markdown and html files
	for rootpath, _, filenames in os.walk(Config.pages_path):
		for filename in filenames:
			if not filename.endswith(".md") and not filename.endswith(".html"):
				continue

			path = rootpath + "/" + filename
			file = open(path, 'r').read()
			raw_file_cache[path] = file

			if filename.endswith(".md"):
				compiled_md_cache[path] = md.convert(file)
				metadata_cache[path] = md.Meta


init_caches()


def post_output_filename(path):
	meta = metadata_cache[path]
	custom_url = meta['url'][0]
	if custom_url != "":
		return "out/" + custom_url + ".html"
	if Config.prependPostId:
		return ("out/" + meta['post_id'][0] + ('-' if Config.spaceToDash else '_') + os.path.basename(path)) \
				   [:-3] + ".html"
	return ("out/" + os.path.basename(path))[:-3] + ".html"


def md_files_in_dir(directory):
	# return list of markdown files in this directory
	files = []
	for rootpath, _, filenames in os.walk(directory):
		for filename in filenames:
			if filename[-3:] != '.md':
				continue

			# https://stackoverflow.com/a/13617120
			rel_dir = os.path.relpath(rootpath, directory)
			rel_file = os.path.join(rel_dir, filename)  # filename relative to content/
			# strip ./ off of rel_file
			if rel_file[:2] == './':
				rel_file = rel_file[2:]

			# get the path relative to ssgen.py (including content/)
			filename_content = os.path.join(directory, rel_file)
			files.append(filename_content)
	return files


def non_md_files_in_dir(directory):
	files = []
	for rootpath, _, filenames in os.walk(directory):
		for filename in filenames:
			if filename[-3:] != '.md':
				# https://stackoverflow.com/a/13617120
				rel_dir = os.path.relpath(rootpath, directory)
				rel_file = os.path.join(rel_dir, filename)  # filename relative to content/
				# strip ./ off of rel_file
				if rel_file[:2] == './':
					rel_file = rel_file[2:]

				# get the path relative to ssgen.py (including content/)
				filename_content = os.path.join(directory, rel_file)
				files.append(filename_content)
	return files


def html_files_in_dir(directory):
	# return list of html files in this directory
	# pretty much same as md_files_in_dir - should refactor
	files = []
	for rootpath, _, filenames in os.walk(directory):
		for filename in filenames:
			if filename[-5:] != '.html':
				continue

			# https://stackoverflow.com/a/13617120
			rel_dir = os.path.relpath(rootpath, directory)
			rel_file = os.path.join(rel_dir, filename)  # filename relative to content/
			# strip ./ off of rel_file
			if rel_file[:2] == './':
				rel_file = rel_file[2:]

			# get the path relative to ssgen.py (including content/)
			filename_content = os.path.join(directory, rel_file)
			files.append(filename_content)
	return files


def md_files_posts():
	# return list of filepaths for posts
	return md_files_in_dir(Config.content_path)


def md_files_pages():
	# return list of filepaths for pages
	return md_files_in_dir(Config.pages_path) + html_files_in_dir(Config.pages_path)


def filepairs_posts():
	# return dict with full markdown file path and full output file path for posts
	files = {}
	files_list = md_files_posts()
	for md_filepath in files_list:
		# filepath for files in subdirectories is "content/subdirectory/filename.md", strip path and .md, and add .html
		outfile = post_output_filename(md_filepath)
		files[md_filepath] = outfile
	return files


def filepairs_pages():
	# return dict with full markdown file path and full output file path for pages
	files = {}
	files_list = md_files_pages()
	for md_filepath in files_list:
		# filepath is like "pages/page.md", strip folder name and .md, add .html
		# unless the page is given as html, then just remove the path
		if md_filepath.endswith(".html"):
			outfile = os.path.join('out', os.path.basename(md_filepath))
		else:
			outfile = os.path.join('out', os.path.basename(md_filepath)[:-3] + ".html")
		files[md_filepath] = outfile
	return files
