from datetime import date, datetime

from python.core.config import Config
from python.core.contentUtils import post_output_filename
from python.core.metaUtils import all_post_ids, pages_info, post_filenames, post_titles

"""
TODO: Improve runtime:
https://stackoverflow.com/a/6117124
https://gist.github.com/bgusach/a967e0587d6e01e889fd1d776c5f3729
"""


def expand_list(html, listname, list):
	list_template_begin_search = "{{begin_list " + listname + "}}\n"
	list_template_end_search = "\n{{end_list " + listname + "}}"
	list_template = html.split(list_template_begin_search)[1].split(list_template_end_search)[0]

	replace_str = ""

	for element in list:
		replace_str += list_template.replace("{{list element}}", element)

	return html.replace("{{place_list " + listname + "}}", replace_str)


def expand_list_multivalue(html, listname, list):
	# expand lists with {{list element filename}} and {{list element title}}
	# the 'list' argument is a list of dicts, with each dict having keys 'filename' and 'title'
	# (or with any other dictionary, each {{list element <key>}} is replaced with that key's value)
	list_template_begin_search = "{{begin_list " + listname + "}}\n"
	list_template_end_search = "\n{{end_list " + listname + "}}"
	try:
		list_template = html.split(list_template_begin_search)[1].split(list_template_end_search)[0]
	except IndexError:
		# this list template isn't in the template HTML file, just return it without replacing anything
		return html

	replace_str = ""

	for element in list:
		this_list_item = list_template
		for key in element.keys():
			this_list_item = this_list_item.replace("{{list element " + key + "}}", element[key])
		replace_str += this_list_item

	return html.replace("{{place_list " + listname + "}}", replace_str)


include_cache = {}


def complete_includes(html):
	# replace {{include filename.html}} with the contents of templates/filename.html
	while "{{include " in html:
		index = html.find("{{include ")
		remainder = html[index + 10:]  # part after "{{include ", 10 is length of that string
		include_name = remainder.split("}}")[0]  # get just the filename.html from {{include filename.html}}
		if include_name in include_cache:
			include_replace = include_cache[include_name]
		else:
			include_replace = open("templates/" + include_name, 'r').read()
			include_cache[include_name] = include_replace
		html = html.replace("{{include " + include_name + "}}", include_replace)
	return html


def basic_templating(template):
	return template.replace("{{site title}}", Config.siteName). \
		replace("{{default author}}", Config.defaultAuthor). \
		replace("{{current year}}", str(datetime.now().year))


template_file_post = open("templates/post.html", 'r').read()


def template_post(md, meta):
	template = template_file_post

	template = complete_includes(template)
	template = basic_templating(template)
	template = template.replace("{{title}}", meta['title'][0])
	template = template.replace("{{content}}", md)
	template = template.replace("{{author}}", meta['author'][0])

	# date
	datearray = meta['date'][0].split("-")
	post_date = date(int(datearray[0]), int(datearray[1]), int(datearray[2]))
	template = template.replace("{{date}}", post_date.strftime(Config.dateFormat))

	# tag list
	if meta['tags'][0].split(" ") != ['']:
		template = template.replace("{{tags prefix}}", Config.tagListPrefix)
	else:
		template = template.replace("{{tags prefix}}", "")
	template = expand_list(template, "tags", meta['tags'][0].split(" "))

	# navigation list
	nav_links = [page_name + ".html" for page_name in Config.postPageLinks.split(" ")]
	nav_dicts = []
	for i in nav_links:
		# for each page to link to, get only that page's filename/title dict from pages_info, and append it to nav_dicts
		# if this throws an error, check that the pages to link to listed in the config file are all present in pages/
		nav_dicts.append([d for d in pages_info if d.get('filename') == i][0])
	template = expand_list_multivalue(template, "navigation", nav_dicts)

	# next/previous post hyperlinks
	this_id = int(meta['post_id'][0])
	this_index = all_post_ids.index(this_id)
	next_id = -1 if this_index == (len(all_post_ids) - 1) else all_post_ids[this_index + 1]
	prev_id = -1 if this_index == 0 else all_post_ids[this_index - 1]
	next_url = post_output_filename(post_filenames[next_id])[4:] if next_id > 0 else ""
	prev_url = post_output_filename(post_filenames[prev_id])[4:] if prev_id > 0 else ""
	if next_url == "":
		next_html = ""
	else:
		next_html = \
			'<a href="' + next_url + '">' + \
			(post_titles[next_id] if Config.sequentialLinksUseTitle else Config.sequentialNextText) + \
			'</a>'
	if prev_url == "":
		prev_html = ""
	else:
		prev_html = \
			'<a href="' + prev_url + '">' + \
			(post_titles[prev_id] if Config.sequentialLinksUseTitle else Config.sequentialPrevText) + \
			'</a>'

	template = template.replace("{{next post}}", next_html)
	template = template.replace("{{prev post}}", prev_html)
	return template


template_file_index = open("templates/index.html", 'r').read()
template_file_page = open("templates/page.html", 'r').read()


def template_page(raw, md, meta, homepage=False, html_file=False):
	# if it's a HTML file, just replace the title and navbar
	if html_file:
		html_title = raw.split("<!--")[1].split("-->")[0]
		result = raw
		result = complete_includes(result)
		result = basic_templating(result)
		result = result.replace("{{title}}", html_title)
		result = result.replace("{{homepage name}}", Config.homepageLinkName)
		# this is exact same as for non-html below - need to refactor
		if not homepage or Config.homepageSelfLink == "1":
			nav_links = [page_name + ".html" for page_name in Config.pagePageLinks.split(" ")]
		else:
			nav_links = [page_name + ".html" for page_name in Config.pagePageLinks.split(" ") if page_name != 'index']
		nav_dicts = []
		for i in nav_links:
			nav_dicts.append([d for d in pages_info if d.get('filename') == i][0])
		result = expand_list_multivalue(result, "navigation", nav_dicts)
		return result

	if homepage:
		template = template_file_index
	else:
		template = template_file_page
	template = complete_includes(template)
	template = basic_templating(template)
	template = template.replace("{{title}}", Config.homepageName if homepage else meta['title'][0])
	template = template.replace("{{homepage name}}", Config.homepageLinkName)
	template = template.replace("{{content}}", md)

	# navbar expansion
	# get the pages to link to from config
	if not homepage or Config.homepageSelfLink == "1":
		nav_links = [page_name + ".html" for page_name in Config.pagePageLinks.split(" ")]
	else:
		nav_links = [page_name + ".html" for page_name in Config.pagePageLinks.split(" ") if page_name != 'index']
	# list of dicts w/ filename, title to pass to expand_list_multivalue
	nav_dicts = []
	for i in nav_links:
		# for each page to link to, get only that page's filename/title dict from pages_info, and append it to nav_dicts
		nav_dicts.append([d for d in pages_info if d.get('filename') == i][0])
	template = expand_list_multivalue(template, "navigation", nav_dicts)
	return template


template_file_tag = open("templates/tag.html", 'r').read()


def template_tag(tagname, posts):
	template = template_file_tag
	template = complete_includes(template)
	template = basic_templating(template)
	template = template.replace("{{title}}", tagname)
	template = expand_list_multivalue(template, "posts",
									  posts if Config.tagPagesNewestFirst else list(reversed(posts)))

	# navbar
	nav_links = [page_name + ".html" for page_name in Config.pagePageLinks.split(" ")]
	nav_dicts = []
	for i in nav_links:
		nav_dicts.append([dict(d) for d in pages_info if d.get('filename') == i][0])
	for i in nav_dicts:
		i['filename'] = "../" + i['filename']
	template = expand_list_multivalue(template, "navigation", nav_dicts)

	# it's kind of a hack to put this here
	template = template.replace('href="index.html"', 'href="../index.html"')
	template = template.replace('href="archives.html"', 'href="../archives.html"')
	template = template.replace('href="editor.html"', 'href="../editor.html"')

	return template


template_file_archive = open("templates/archive.html", 'r').read()


def template_archive(posts):
	template = template_file_archive
	template = complete_includes(template)
	template = basic_templating(template)
	template = template.replace("{{title}}", "Archives")
	template = expand_list_multivalue(template, "posts",
									  posts if Config.archiveNewestFirst else list(reversed(posts)))

	# navbar
	nav_links = [page_name + ".html" for page_name in Config.pagePageLinks.split(" ")]
	nav_dicts = []
	for i in nav_links:
		nav_dicts.append([d for d in pages_info if d.get('filename') == i][0])
	template = expand_list_multivalue(template, "navigation", nav_dicts)

	return template
