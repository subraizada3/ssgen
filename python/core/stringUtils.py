from python.core.config import Config


class StringUtils:
	@staticmethod
	def toFileName(title: str):
		allowed_chars = "abcdefghijklmnopqrstuvwxyz"
		allowed_chars += allowed_chars.upper()
		allowed_chars += "1234567890"
		allowed_chars += "-_"  # it may or may not be safe to add in $.+!*'(),
		chars_to_keep = set(allowed_chars)
		spaceReplacer = '-' if Config.spaceToDash else '_'
		title = title.replace(' ', spaceReplacer).lower()
		# https://stackoverflow.com/a/15754603
		return ''.join(c for c in title if c in chars_to_keep)
