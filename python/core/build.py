########################################################################################################################
###   PRE-BUILD TASKS   ################################################################################################
########################################################################################################################
import os
import shutil
import subprocess

# pre-build pre-clean hook
if os.path.exists('python/pre_clean_hook.sh'):
	subprocess.call(['python/pre_clean_hook.sh'])

# clean output directory, or make it if it doesn't exist
if not os.path.exists('out'):
	os.makedirs('out')
else:
	shutil.rmtree('out')
	os.makedirs('out')

os.makedirs('out/tags')

# copy all non-HTML files from templates/ to out/
for _, _, filenames in os.walk('templates'):
	for filename in filenames:
		if not filename.endswith(".html"):
			shutil.copy("templates/" + filename, "out/" + filename)

# pre-build hook
if os.path.exists('python/pre_hook.sh'):
	subprocess.call(['python/pre_hook.sh'])

########################################################################################################################
###   BUILD   ##########################################################################################################
########################################################################################################################
from python.core.contentUtils import filepairs_posts, filepairs_pages, non_md_files_in_dir, post_output_filename
from python.core.contentUtils import raw_file_cache, compiled_md_cache, metadata_cache
from python.core.config import Config
from python.core.templateUtils import template_post, template_page, template_tag, template_archive
from python.core.metaUtils import post_filenames, all_post_ids

# makes the Markdown object (md) with extensions loaded
from python.core.md_builder import md, footnote_extension

###   build pages   ####################################################################################################
for in_filename, out_filename in filepairs_posts().items():
	footnote_extension.reset()
	# parse markdown from input file & fill in metadata
	in_file = raw_file_cache[in_filename]
	meta = metadata_cache[in_filename]
	md = compiled_md_cache[in_filename]
	mdHtml = template_post(md, meta)

	# warn if file is being overwritten with duplicate
	if os.path.exists(out_filename):
		print("Duplicate file - overwriting: {}".format(out_filename))
	# write out HTML
	out_file = open(out_filename, 'w')
	out_file.write(mdHtml)

# copy non-md files (resources) from content/ to out/
for filename in non_md_files_in_dir(Config.content_path):
	output_path = "out/" + os.path.basename(filename)
	shutil.copyfile(filename, output_path)

###   build posts   ####################################################################################################
all_pages = filepairs_pages()

# process homepage separately
footnote_extension.reset()
# if it is html, copy to output folder
if (Config.pages_path + "/index.html", "out/index.html") in all_pages.items():
	all_pages.pop(Config.pages_path + "/index.html")
	open("out/index.html", 'w').write(template_page(raw_file_cache[Config.pages_path + "/index.html"], "", None,
													homepage=True, html_file=True))
# if it is md, convert to html and put in output folder
elif (Config.pages_path + "/index.md", "out/index.html") in all_pages.items():
	all_pages.pop(Config.pages_path + "/index.md")
	open("out/index.html", 'w').write(template_page(raw_file_cache[Config.pages_path + "/index.md"],
													compiled_md_cache[Config.pages_path + "/index.md"], None,
													homepage=True))

# if it is neither, just copy it to output
for _, _, filenames in os.walk(Config.pages_path):
	for filename in filenames:
		if not filename.endswith('.md') and not filename.endswith('.html'):
			shutil.copyfile(Config.pages_path + "/" + filename, "out/" + filename)

# process all other pages
for in_filename, out_filename in all_pages.items():
	footnote_extension.reset()
	# warn if file is being overwritten with duplicate
	if os.path.exists(out_filename):
		print("Duplicate file - overwriting: {}".format(out_filename))
	if in_filename.endswith(".html"):
		mdHtml = template_page(raw_file_cache[in_filename], "", None, html_file=True)
	else:
		mdHtml = template_page(raw_file_cache[in_filename], compiled_md_cache[in_filename], metadata_cache[in_filename])
	out_file = open(out_filename, 'w')
	out_file.write(mdHtml)

########################################################################################################################
###   GENERATE PAGES   #################################################################################################
########################################################################################################################
###   TAG & ARCHIVE PAGES   ############################################################################################
# list of tags & associated pages
tags = {}  # keys are tag name, value is list of dicts w/ keys 'filename' and 'title'
# list of all posts, for archive
all_posts = []

# iterate over this in order of post ID, to get ordered archives
for i in all_post_ids:
	in_filename = post_filenames[i]
	out_filename = post_output_filename(in_filename)
	meta = metadata_cache[in_filename]
	# perform modifications to out_filename if necessary (same as in the build step)
	# if URL specified in metadata, set output filename to that instead
	if meta['url'][0] != "":
		url = meta['url'][0]
		out_filename = "out/" + url + ".html"
	# otherwise, if specified in config, add post ID to output filename
	elif Config.prependPostId:
		out_filename = out_filename.replace("out/",
											"out/" + meta['post_id'][0] + ('-' if Config.spaceToDash else '_'))

	post_filename = out_filename[4:]  # remove "out/" from the output filename
	post_title = meta['title'][0]

	all_posts.append({'filename': post_filename, 'title': post_title})

	post_tags = meta['tags'][0].split(" ")
	if post_tags == ['']:
		continue

	for tag in post_tags:
		# put this tag into tags if it doesn't already exist
		try:
			tags[tag]
		except KeyError:
			tags[tag] = list()
		tags[tag].append({'filename': post_filename, 'title': post_title})

if Config.tagsEnabled:
	for tag, posts in tags.items():
		tag_filename = "out/tags/" + tag + ".html"
		tag_file = open(tag_filename, 'w')
		tag_file.write(template_tag(tag, posts))

if Config.archiveEnabled:
	archive_file = open('out/archives.html', 'w')
	archive_file.write(template_archive(all_posts))

########################################################################################################################
###   POST-BUILD TASKS   ###############################################################################################
########################################################################################################################
# if editor is enabled, copy it into out/
if Config.markdownEditor:
	for rootpath, _, filenames in os.walk('python/simplemde'):
		for filename in filenames:
			shutil.copy(rootpath + "/" + filename, "out/" + filename)

# post-build hook
if os.path.exists('python/post_hook.sh'):
	subprocess.call(['python/post_hook.sh'])
