import os
from _bisect import insort

from markdown import Markdown

from python.core.config import Config
from python.core.contentUtils import filepairs_pages, filepairs_posts, metadata_cache, raw_file_cache

"""
Provided functions and variables:

all_post_ids: list of all post IDs present
max_post_id: number of greatest post ID present

post_filenames: dict of ID to full filename (including content/) for all posts
post_titles: dict of ID to title for all posts

pages_info: list of dicts with 'filename' (without path) and 'title' for every page
"""

md = Markdown(extensions=['markdown.extensions.meta'])

all_post_ids = []


def get_max_post_id():
	max_id = 0
	for in_filename, _ in filepairs_posts().items():
		this_post_id = int(metadata_cache[in_filename]['post_id'][0])
		insort(all_post_ids, this_post_id)
		if this_post_id > max_id:
			max_id = this_post_id
	return max_id


# for some reason just doing all_post_ids.sort() or all_post_ids = sorted(all_post_ids) over here doesn't result in
#     imported all_post_ids begins sorted, so bisect.insort is used in the for loop
max_post_id = get_max_post_id()


def get_pages_info():
	# returns list of dicts, each dict represents a page and has keys like:
	# filename: "index.html", title: "Home Page"
	result = list()
	for in_filename, _ in filepairs_pages().items():
		# homepage doesn't have metadata, add it manually
		if in_filename.endswith('index.md') or in_filename.endswith('index.html'):
			result.append({'filename': 'index.html', 'title': Config.homepageLinkName})
			continue

		# pages given as HTML have title in comment on first line
		if in_filename.endswith('.html'):
			page_title = raw_file_cache[in_filename].split("<!--")[1].split("-->")[0]
			result.append({'filename': os.path.basename(in_filename), 'title': page_title})
			continue

		page_info = {'filename': os.path.basename(in_filename)[:-3] + ".html",
					 'title': metadata_cache[in_filename]['title'][0]}
		result.append(page_info)
	return result


pages_info = get_pages_info()


def get_post_by_id(id):
	for in_filename, _ in filepairs_posts().items():
		if int(metadata_cache[in_filename]['post_id'][0]) == id:
			return in_filename


def get_post_title_by_id(id):
	for in_filename, _ in filepairs_posts().items():
		if int(metadata_cache[in_filename]['post_id'][0]) == id:
			return metadata_cache[in_filename]['title'][0]


post_filenames = {}
post_titles = {}
for i in all_post_ids:
	post_filenames[i] = get_post_by_id(i)
	post_titles[i] = get_post_title_by_id(i)
