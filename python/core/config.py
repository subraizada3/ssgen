from configparser import RawConfigParser


class Config:
	# content_path = 'test_content'
	# pages_path = 'test_pages'

	content_path = 'content'
	pages_path = 'pages'

	cp = RawConfigParser()
	cp.read('config.ini')
	siteName = cp['base']['SiteName']
	defaultAuthor = cp['base']['DefaultAuthorName']
	homepageName = cp['base']['HomepageName']
	homepageLinkName = cp['base']['HomepageLinkName']
	spaceToDash = cp.getboolean('base', 'SpaceToDash')
	prependPostId = cp.getboolean('base', 'PrependPostID')
	prependPostIdFilename = cp.getboolean('base', 'PrependPostIDFilename')
	markdownEditor = cp.getboolean('base', 'Editor')
	pagePageLinks = cp['nav']['PageToPageLinks']
	homepageSelfLink = cp['nav']['HomepageSelfLink']
	postPageLinks = cp['nav']['PostToPageLinks']
	archiveEnabled = cp.getboolean('nav', 'ArchiveEnabled')
	archiveNewestFirst = (cp['nav']['ArchiveOrder'] == 'newest')
	tagsEnabled = cp.getboolean('nav', 'TagPagesEnabled')
	tagPagesNewestFirst = (cp['nav']['TagOrder'] == 'newest')
	tagListPrefix = cp['nav']['TagListPrefix'].strip('"')
	tocTitle = cp['appearance']['TocTitle']
	dateFormat = cp['appearance']['DateFormat']
	sequentialLinksUseTitle = cp.getboolean('appearance', 'SequentialLinksUseTitle')
	sequentialNextText = cp['appearance']['SequentialLinksNextText']
	sequentialPrevText = cp['appearance']['SequentialLinksPrevText']
