import sys

from python.core.stringUtils import StringUtils

# get metadata info from user
title = input("Page title\n")
if title == "":
	print("Title cannot be blank")
	sys.exit(0)
url = input("Page URL (example.com/page.html -> enter 'page') (leave empty to autogenerate)\n")

# open the file
pageFilePath = "pages/" + StringUtils.toFileName(title) + ".md"
try:
	f = open(pageFilePath, 'x')
except FileExistsError:
	print("{} already exists".format(pageFilePath))
	sys.exit(0)

# write out page template
f.write("Title: " + title)
f.write("\nURL:   " + url)
f.write("\n\n[comment]: # (Make sure to leave an empty line above and below this)\n\n")

print("{} has been generated".format(pageFilePath))
