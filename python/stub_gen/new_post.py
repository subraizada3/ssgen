import sys
from datetime import datetime

from python.core import metaUtils
from python.core.config import Config
from python.core.stringUtils import StringUtils

# get metadata info from user
title = input("Post title\n")
if title == "":
	print("Title cannot be blank")
	sys.exit(0)

url = input("Post URL (example.com/post.html -> enter 'post') (leave empty to autogenerate)\n")

author = Config.defaultAuthor
author_override = input("Enter author to override default ({}):\n".format(Config.defaultAuthor))
if author_override != "":
	author = author_override

date = datetime.today().strftime("%Y-%m-%d")
date_override = input("Enter date to override today (YYYY-MM-DD)\n")
if date_override != "":
	date = date_override
	pass

tags = input("Enter tags, separated by spaces. Use only letters and - or _\n")

post_id = metaUtils.max_post_id + 1
post_id_override = input("Enter post id to override auto-detected ({})\n".format(post_id))
if post_id_override != "":
	post_id = post_id_override

# open the file
postFilePath = StringUtils.toFileName(title) + ".md"
if Config.prependPostIdFilename:
	postFilePath = str(post_id) + ('-' if Config.spaceToDash else '_') + postFilePath

try:
	f = open(postFilePath, 'x')
except FileExistsError:
	print("{} already exists".format(postFilePath))
	sys.exit(0)

# title, author, date, tags, ID, URL
# write out post template
f.write("Title:   " + title)
f.write("\nURL:     " + url)
f.write("\nAuthor:  " + author)
f.write("\nDate:    " + date)
f.write("\nTags:    " + tags)
f.write("\nPost_ID: " + str(post_id))
f.write("\n\n[comment]: # (Make sure to leave an empty line above and below this)\n\n")

print("{} has been generated".format(postFilePath))
