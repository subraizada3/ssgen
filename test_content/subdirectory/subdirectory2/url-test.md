Title:   URL Test
URL:     custom_url
Author:  Sub Raizada
Date:    2018-03-18
Tags:    cool_post
Post_ID: 7

[comment]: # (Make sure to leave an empty line above and below this)

# Test for custom URLs

# And also images now:
With HTML width:

<img src="7-cat.jpeg" alt="cat picture" width="100px">

And just plain markdown:

![cat picture](7-cat.jpeg)
