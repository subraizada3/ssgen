Title:   First post
URL:     
Author:  Sub Raizada
Date:    2018-03-09
Tags:    tag1 cool_post good-post
Post_ID: 1

[comment]: # (Make sure to leave an empty line above and below this)

## Stuff that doesn't work:

 * strikethrough - not supported by python-markdown

## Testing area
### Table of contents
[TOC]

``this is inline code``

and

````python
for in_filename, out_filename in filepairs_posts().items():
	# parse markdown from input file & fill in metadata
	in_file = open(in_filename, 'r')
	mdHtml = template_post(in_file, md)
````

````
this
is
block
code
````

> this is a quote which goes off the side of the page aaaaaaaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaaaaaaaa

> continued here

Quote break...

Some 'words' here

And "angle" brackets: << >>

Various dashes: - -- ---

> here's another quote

And some <kbd>inline html</kbd>, along with *italic* and **bold**

A sentence with ~~strikethrough~~.

  * inline lists work
      * more
  * more
      * more
          * more
      * more

 1. ordered lists
 2. more order
     2. even more
         3. very much more
             4. more

### Now, on to the main article:
---


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ullamcorper, libero eget eleifend dignissim, ligula enim aliquam purus, eget porttitor orci felis a leo. Nam venenatis lorem non libero interdum dictum. Maecenas felis libero, condimentum nec cursus sit amet, convallis scelerisque justo. Etiam elementum elit mauris. In varius nunc in justo rhoncus, in mattis libero rutrum. Donec est metus, maximus a velit quis, dapibus eleifend neque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec turpis odio, egestas sit amet laoreet quis, pellentesque sed dui. Cras placerat ullamcorper suscipit. Praesent lacinia bibendum dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis massa non odio efficitur varius. In consectetur egestas velit sit amet iaculis.

Donec sodales pretium velit quis vulputate. Nam non mattis massa. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas maximus metus ac volutpat ornare. Vestibulum non nunc tristique, gravida leo sit amet, eleifend purus. Suspendisse efficitur velit blandit sem efficitur pulvinar. Ut pellentesque justo at enim dignissim facilisis. Aliquam sed enim commodo enim facilisis suscipit. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque condimentum libero sed dolor imperdiet, sed condimentum tellus tempor. Sed at rutrum nunc. Curabitur vulputate sem eget eleifend lacinia.

Donec mollis lectus at enim feugiat mattis. In risus ex, pellentesque eget erat a, tempor tempus purus. Etiam molestie tellus nec aliquet iaculis. Sed non tortor ac sapien ullamcorper ultrices. Curabitur auctor ante ut dolor tempor accumsan. Proin non felis tellus. Praesent id lorem lorem. Nam vehicula ante nec neque facilisis, sed interdum purus pulvinar. Nunc aliquam rutrum finibus. Mauris rutrum ipsum ut malesuada tincidunt. Aenean vel metus magna. Donec a hendrerit odio, sed posuere neque. Ut pretium nisl nec luctus molestie.

Etiam pharetra luctus dignissim. Fusce augue odio, mollis ac fringilla a, molestie at purus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus non ultricies purus. Nam quis rhoncus leo. Nam euismod lorem eu nunc mollis pretium. Nam eu lectus vel augue dictum molestie. Mauris in massa sagittis, elementum turpis in, fringilla enim. Praesent at urna ac orci ultrices dapibus nec vitae tortor. Cras dignissim odio interdum, consectetur ante quis, tempus lacus. Pellentesque pharetra turpis eu molestie blandit. Sed vitae lectus et nulla blandit varius vel ut ex. Curabitur nec leo ac velit ultricies finibus.

Suspendisse potenti. Nam sagittis orci lorem, in luctus sapien condimentum in. Vestibulum vulputate lectus at justo lacinia congue. Praesent quis leo efficitur, posuere nunc sed, viverra urna. Mauris commodo sem augue, eget egestas sem porta id. Nullam mollis consectetur ligula sed elementum. Nulla facilisi. 
