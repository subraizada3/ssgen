Title: Quick Start
URL:   

[comment]: # (Make sure to leave an empty line above and below this)

### Get the program
`cd` to the root folder of the extracted ssgen zip [(download)](https://bitbucket.org/subraizada3/ssgen/get/HEAD.zip)

`rm -r content/* pages/* test_content/ test_pages/ .gitignore README.md` will delete this guide, and development files unnecessary for running the program, from your local copy of ssgen and leave you with a blank site

Install the necessary Python libraries:
`pip install Markdown Pygments` or `pacman -S ptyhon-markdown python-pygments`

The default installation has various plugins enabled by default, either install their requirements or comment them out in `python/core/md_builder.py`.

````
pip install mdx_truly_sane_lists pymdown-extensions
````

**Note**: A default enabled extension makes the markdown list indent *2 spaces* instead of *4 spaces*.

### Set site metadata
Sane defaults are present in the config (`config.ini`), but a few changes still need to be made (such as the site name and author name).
These are listed at the top of the file.

Page-page links and post-page links specify what pages appear in the navigation bar when viewing a page or post.
For example, this site uses `full-documentation quick-start` for both options.

A reasonable default for both options is `about`, this results in every post/page on your site linking to the About page from the navigation bar.
If you add in more pages there, make sure they exist in the `pages` folder, or the build will fail.

### Create content
The homepage is required, make `index.md` or `index.html` in the `posts` folder, and put content into it if you want. This should be a pure markdown or HTML file.

Every other post or page has some required metadata before the markdown starts, the ssgen script can generate stub files with the metadata filled in:

- `./ssgen.py new` or `./ssgen.py post` to create a new post, and then `mv` it into `content`
  - Organize your content directory however you want; all files are unrolled into a single, flat `out` folder.
  - Images and other resources can be placed in subdirectories of `content`, but make sure the link in your markdown file refers to just the filename, without any subdirectory.
- `./ssgen.py page` to create a new page; it will be placed into `pages`
  - Or, if you want to give a specific page as HTML, just put the HTML file into `pages`.
    - For HTML pages, the first line must be `<!--Page Title-->`.

Place content into the generated markdown files.

### Publish it
````
:::bash
./ssgen.py               # build the site
firefox out/index.html   # take a look at it
# publish it! (this can be easily automated)
cp -r out/ server-dir/
scp -r out/* user@example.com:/srv/www
````

For automatic publishing, take a look at the provided publish script (mentioned at the top of the [full documentation](full-guide.html)) and the [post build hooks](full-guide.html#build-hooks).

**Warning**: do not put anything important into `out`, it is deleted before every build.

### More advanced features and options
[In-Depth Guide](full-guide.html)
