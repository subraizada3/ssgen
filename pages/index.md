*Freaky fast website delivery: from scratch to a site like this in 60 seconds.*

## Requirements:
- Running Linux/UNIX (macOS may or may not work)
- Python 3

## Download:
Get the
[zip](https://bitbucket.org/subraizada3/ssgen/get/HEAD.zip)
and then follow the
[quick start guide](quick-start.html)

## Sample pages:
[Lorem ipsum text](lorem-ipsum-2.html)

[Markdown features](markdown-demo.html)

[Tag collection](tags/second-tag.html)

[Archives page](archives.html)

---

Source code on Bitbucket: [https://bitbucket.org/subraizada3/ssgen](https://bitbucket.org/subraizada3/ssgen)
