# EXTENSIONS
# Note on emoji:
#   unimoji supports natural emojis like :)
#   pymdownx.emoji supports emoji tags like :smile: and :pig:
#   They both conflict with each other; only one can be enabled.
#   Neither are enabled by default; pick one if you want emoji support.

# configure footnote extension
# use text ↩︎︎ character instead of emoji: https://alexwlchan.net/2017/03/extensions-in-python-markdown/
footnote_extension = FootnoteExtension(configs={
	'BACKLINK_TEXT': '&#8617;&#xFE0E;'
})

# remove footnote extension from extra so it isn't double-included when including extra
extra.extensions.remove('markdown.extensions.footnotes')  # prevent footnotes from being included again by extra
extra.extensions.remove('markdown.extensions.smart_strong')  # conflicts with pymdownx.betterem
extra_extension = extra.ExtraExtension()

md = Markdown(extensions=['markdown.extensions.meta', 'markdown.extensions.sane_lists', 'markdown.extensions.toc',
						  'markdown.extensions.codehilite', 'markdown.extensions.smarty',
						  extra_extension, footnote_extension
						  # @formatter:off

# UNCOMMENT ADDITIONAL EXTENSIONS TO USE HERE
# make sure not to delete the leading comma

# Unenabled Python-Markdown extensions
#   Documentation: https://python-markdown.github.io/extensions/
# , 'markdown.extensions.admonition' # allows for rST style admonitions
# , 'markdown.extensions.nl2br' # newlines are converted to <br>, don't need to leave an empty line
# , 'markdown.extensions.wikilinks' # [[bracketed]] words get converted to WikiLinks

# Other available extensions
, 'mdx_truly_sane_lists' # makes lists even more sane; pip install mdx_truly_sane_lists
# , 'superscript' # text in ^carets^ is superscripted; pip install MarkdownSuperscript
# , 'subscript' # text in ~tildes~ is subscripted; pip install MarkdownSubscript
# , 'del_ins' # text in ++2 pluses++ and ~~2 tildes~~ is wrapped in html <ins> and <del> pip install mdx_del_ins
#      the functionality of superscript, subscript, and del_ins is also implemented in pymdownx.caret and tilde
# , 'unimoji' # auto-generates emoji from smileys :) that have whitespace on each side; pip install mdx_unimoji
#      this conflicts with pymdownx.emoji

# pymdown extensions collection - pip install pymdown-extensions
#   documentation at https://facelessuser.github.io/pymdown-extensions/

# First, extensions that can conflict with other default extensions:
, 'pymdownx.betterem' # improves bold and italics usage
#      conflicts with markdown.extensions.smartstrong (in markdown.extensions.extra) (see line 53)
# , 'pymdownx.extra' # includes the following extensions
#      includes markdown.extensions.extra, magiclink, betterem, tilde, emoji, tasklist, superfences
# , 'pymdownx.github' # includes the following extensions to behave like github flavored markdown
#      includes markdown.extensions.tables (in markdown.extensions.extra), magiclink, betterem, tile, emoji, tasklist, superfences

# And pymdown extensions that may conflict with other commented extensions above:
, 'pymdownx.caret' # text in ^^2 carets^^ gets html <ins> and text in ^a caret^ gets superscripted
#      may conflict with 'del_ins' and 'superscript' above
, 'pymdownx.tilde' # text in ~~2 tildes~~ gets html <del> and text in ~a tilde~ gets subscripted
#      may conflict with 'del_ins' and 'subscript' above
# , 'pymdownx.emoji' # allows easy addition of emoji
#      this conflicts with
# , 'pymdownx.superfences' # fences in code blocks
#      conflicts with markdown.extensions.fenced_code

# And finally, conflict-less extensions:
, 'pymdownx.arithmatex' # allows for usage of LaTeX equations with MathJax
#      need to uncomment the MathJax <script> tag in templates/html-head.html
# , 'pymdownx.b64' # embeds local images with base64 encoding
# , 'pymdownx.critic' # adds Critic Markup support
# , 'pymdownx.details' # adds <details><summary> tags
# , 'pymdownx.escapeall' # can escape any character with \
# , 'pymdownx.extrarawhtml' # no idea what this does
# , 'pymdownx.highlight' # allows for configuration of superfences and inlinehilite
, 'pymdownx.inlinehilite' # highlights `inline code`
, 'pymdownx.keys' # allows using ++pluses++ instead of <kbd></kbd>, also works for shortcuts like ++ctrl+alt+delete++
, 'pymdownx.magiclink' # automatically linkifies URLs and emails
, 'pymdownx.mark' # allows for html <mark> (highlighter) using ==2 equals signs==
# , 'pymdownx.pathconverter' # does something with link paths
# , 'pymdownx.progressbar' # allows adding progress bars with [=65% "optional title"]
, 'pymdownx.smartsymbols' # automatically symbolizes text like (c), =/=, 1/4, -->, etc.
# , 'pymdownx.snippets' # allows including other markdown/HTML files
, 'pymdownx.tasklist' # adds github-style tasklists
# , 'pymdownx.striphtml' # strips comments from compiled markdown
# @formatter:on
						  ],
			  extension_configs={
				  'markdown.extensions.smarty': {
					  'smart_angled_quotes': 'True'
				  },
				  'markdown.extensions.toc': {
					  'title': Config.tocTitle
				  }
			  })

# END EXTENSIONS
