#!/usr/bin/env python
# may need to change the above line to 'python3' on some systems

import os
import sys

# change working directory to this script's containing folder
# https://stackoverflow.com/a/1432949
abspath = os.path.abspath(__file__)
dirname = os.path.dirname(abspath)
os.chdir(dirname)

# call the script to make a new post/page, or to build
if len(sys.argv) > 1:
	argName = sys.argv[1]
	if argName == 'new' or argName == 'post':
		exec(open('python/stub_gen/new_post.py').read())
	elif argName == 'page':
		exec(open('python/stub_gen/new_page.py').read())
	elif 'help' in argName or '-h' in argName:
		print("Run ./ssgen.py to build the website.\n"
			  "To generate a new post, run ./ssgen.py new or ./ssgen.py post,\n"
			  "    and then copy <newpost>.md into the pages/ directory\n"
			  "To generate a new page, run ./ssgen.py page, and edit pages/<newpage>.md")
else:
	exec(open('python/core/build.py').read())
