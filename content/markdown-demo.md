Title:   Markdown Demo
URL:     
Author:  Sub Raizada
Date:    2018-03-19
Tags:    
Post_ID: 4

[comment]: # (Make sure to leave an empty line above and below this)

This is `inline code`, and here is
````python
"block code with (configurable) syntax highlighting"
for i in range(2, 5):
	print('abc')
````

You can make text **bold** or *italic* or ***both***.
Or if it's more your ==style==, ~~strikethrough~~ some words, and maybe throw in some super^scripts^ and sub~scripts~ too.

'Quotes' appear "nicely," and so do << >> <-- --> <---> arrows, without any extra typing. Symbols are also automatically converted from text to actual symbols: (c) (r) 1/4 =/= was typed as `(c) (r) 1/4 =/=`.

There are two plugins available for emoji; one converts <kbd>:)</kbd> these <kbd>:P</kbd>, and the other converts tags like <kbd>:smiley:</kbd> and <kbd>:pig:</kbd>.

LaTeX math works: $k_n \cdot \sum_{i=0}^{10}{(a+b)} + \int_{a}^{b}{f(x)}$

And the same thing expanded:

$$
k_n \cdot \sum_{i=0}^{10}{(a+b)} + \int_{a}^{b}{f(x)}
$$

I haven't tried it, but AsciiMath and MathML should work too.

> there are quotes that wrap when they are very very long aaaaa aaaaa aaaaa aaaaa aaaaa aaaaa aaaaa aaaaa aaaaa aaaaa aaaaa aaaaa aaaaa aaaaa aaaaa aaaaa aaaaa aaaaa aaaaa
> > and you can nest them too

> which is pretty cool

There  | are    | tables
------:|:------:|:------
That   | you    | can
`make`   | with   | alternating
row    | colors | and
**custom** | alignment |

<hr>

* bulleted
  * lists
    * that
      * can
    * go
  * to
* arbitrary depths

<hr>

1. And
  1. Numbered
    1. Lists
1. Also
  1. Work

1. Of course,
  * They can be
  * mixed
  
  1. Without any
  1. extra effort

<hr>

Many extensions to the markdown parser are enabled by default, and more can be easily enabled or disabled in a configuration file. Here are some of the features of the default setup:

# Headings
## At
### Multiple
#### Levels
##### Which go to
# The standard
###### h6

And a table of contents auto-generated from headings

[TOC]

<hr>

It's easy to add keyboard shortcuts: `++ctrl+alt+delete++` gives ++ctrl+alt+delete++, or you can use the standard HTML `<kbd>` tags: <kbd>a</kbd>+<kbd>b</kbd>=<kbd>c</kbd>

Standard markdown [links](index.html) and images:

![bear](bear.jpg).

Links can also go to [headings on the page](#headings).

HTML can be used within the content files - the horizontal lines

---

on this page are written as `---` on a line when using markdown, but I put in `#!html <hr>` tags instead.
