Title:   Post with a tag - example one
URL:     
Author:  Sub Raizada
Date:    2018-03-19
Tags:    tag_one second-tag
Post_ID: 2

[comment]: # (Make sure to leave an empty line above and below this)

Note the two tags linked above

In the metadata for this post:
`Tags:    tag_one second-tag`

Also note the links to the next/previous post below (sorted by post ID)
